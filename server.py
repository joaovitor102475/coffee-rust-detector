from flask import Flask, redirect, render_template, request
from flask import *
import os 
from werkzeug.utils import secure_filename
from PIL import Image
from tensorflow import keras
import numpy as np

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = os.getcwd() + '/cache'

def predict(altura_largura, path_to_model, file):       
    model = keras.models.load_model(path_to_model)
    
    img = Image.open(file)
    img = img.resize(altura_largura, Image.ANTIALIAS)
    imgArray = np.array(img)
    x = imgArray

    x = np.expand_dims(x, axis=0)
    pred=(model.predict(x) > 0.5).astype('int32')[0][0]
    prediction = model.predict(x)

    if pred == 1:
        text = '''
        Sua planta está saudável! Abaixo segue as recomendações:
        1 - A ferrugem ama condições úmidas, portanto evite regar em excesso suas plantas. Use uma mangueira giratória ou irrigação por gotejamento para manter a água fora da folhagem. Como bônus, irrigar por gotejamento vai fornecem água de maneira mais eficiente do que o aspersor ou bicos de pulverização, economizando dinheiro em suas contas de água.
        2 - Se você regar sobre as folhas, faça isso no início do dia, então a folhagem tem tempo de secar antes do anoitecer.
        3 - Procure por plantas resistentes à ferrugem para cultivar. No entanto, esteja ciente de que alguns dos patógenos fúngicos que causam a ferrugem podem sofrer mutação, e estas plantas podem ser afetadas de qualquer forma. 
        4 - Certifique-se de que qualquer nova planta que você compre ou traga para casa esteja livre de doença. Em caso de dúvida, isole-as do resto do seu jardim por cerca de três semanas para ter certeza de que estão saudáveis.
        5 - Quando você for plantar, espaçar as plantas como recomendado. Boa circulação de ar pode ajudar a prevenir muitas doenças.gumas formas de previnir essa doença:
        '''
        return 'SAUDÁVEL', prediction, text
    else:
        text = '''
        Parece que sua planta está doente!
        A doença causada por Hemileia vastatrix provoca queda precoce das folhas e seca dos ramos, afetando a produção de frutos do ano seguinte. É um patógeno que provoca prejuízos durante os anos de alta produção da cultura.

        O fungo possui distribuição mundial. No Brasil, foi relatado inicialmente no estado da Bahia e posteriormente disseminou-se para todas as regiões produtoras de café do país. É considerada, mundialmente, a doença mais importante da cultura. H. vastatrix não apresenta hospedeiros intermediários para os estádios de pícnio e écio, e no cafeeiro ocorrem os estádios de urédia, télia e basídio.

        Até o ano de 2001 foram constatadas 33 raças do fungo, sendo que no Brasil predomina a raça II, mas já foram detectadas as raças I, II, III, VII, XV, XVI, XVII, XVIII, XXIV, XXV, XXX e XXXI em Coffea arabica.

        Danos: Em média, as perdas ficam em 35% quando as condições climáticas são favoráveis ao desenvolvimento da doença, mas, quando coincidem períodos de estiagem prolongada com alta incidência do fungo, os prejuízos podem ultrapassar 50%.

        Os sintomas da doença surgem na face inferior da folha. No início, as manchas são pequenas, variando de 1 a 3 mm de diâmetro, com coloração amarelo-pálida. Posteriormente, as manchas podem atingir até 2 cm de diâmetro e passam a exibir coloração amarelo-alaranjada e aspecto pulverulento. Nessa fase é possível observar lesões cloróticas amarelas na face superior da folha, correspondentes às pústulas existentes na face inferior. Essas lesões aumentam de tamanho, e o centro adquire aspecto necrótico.

        Controle:  O uso de cultivares resistentes é o melhor método de controle.

        Os fungicidas de contato, principalmente os cúpricos, e os sistêmicos podem ser utilizados para o controle preventivo da doença. Recomenda-se efetuar alternância entre fungicidas de contato e sistêmicos. O emprego de fungicidas sistêmicos pode ser via foliar ou via solo. A calda viçosa, uma mistura de nutrientes que apresenta efeito fungicida, também é utilizada no controle. Sempre usar produtos que sejam registrados para as culturas.
        '''
        return 'DOENTE', prediction, text

@app.route('/', methods=["GET"])
def homepage():
    return render_template("homepage.html")

@app.route('/resultados.html', methods=['GET','POST'])
def resultados():
    cache_folder = os.getcwd() + '/cache'
    model_path = os.getcwd() + './model1.keras'

    result = request.files['file']

    for d, sd, arquivos in os.walk(cache_folder):
        lista = arquivos
    for name in lista:
        os.remove(os.path.join(cache_folder + '/', name))

    name = secure_filename(result.filename)
    result.save(os.path.join(app.config['UPLOAD_FOLDER'], name))

    result, valor_predict, text = predict((128, 128), model_path, cache_folder + '/' + name)

    return render_template("resultados.html", result=result, valor_predict=valor_predict, text=text)


if __name__=="__main__":
    app.run(debug=True)